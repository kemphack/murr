from rest_framework import serializers
from django.conf import settings

from .models import Comment


class ChildSerializer(serializers.ModelSerializer):
    author_username = serializers.CharField(source="author.username", read_only=True)
    author_avatar = serializers.SerializerMethodField()
    children = serializers.SerializerMethodField()
    is_like = serializers.SerializerMethodField()
    is_dislike = serializers.SerializerMethodField()

    class Meta:
        model = Comment
        fields = (
            "id",
            "author",
            "author_avatar",
            "author_username",
            "parent",
            "murr",
            "text",
            "rating",
            "created",
            "deleted",
            "children",
            "is_like",
            "is_dislike",
        )
        read_only_fields = ("rating", "created", "author")

    @property
    def user(self):
        request = self.context.get("request")
        return request.user if request else None

    def get_children(self, parent):
        queryset = parent.get_children()
        serializer = ChildSerializer(queryset, many=True, read_only=True, context=self.context)
        return serializer.data

    def get_author_avatar(self, obj):
        if obj.author.murren_avatar and hasattr(obj.author.murren_avatar, "url"):
            return f"{settings.BACKEND_URL}{obj.author.murren_avatar.url}"

    def get_is_like(self, obj) -> bool:
        return obj.liked_murrens.filter(id=self.user.id).exists() if self.user else False

    def get_is_dislike(self, obj) -> bool:
        return obj.disliked_murrens.filter(id=self.user.id).exists() if self.user else False


class CommentSerializer(serializers.ModelSerializer):
    author_username = serializers.CharField(source="author.username", read_only=True)
    author_avatar = serializers.SerializerMethodField()
    children = serializers.SerializerMethodField()
    is_like = serializers.SerializerMethodField()
    is_dislike = serializers.SerializerMethodField()

    class Meta:
        model = Comment
        fields = (
            "id",
            "author",
            "author_username",
            "author_avatar",
            "parent",
            "murr",
            "text",
            "rating",
            "created",
            "deleted",
            "children",
            "is_like",
            "is_dislike",
        )
        read_only_fields = ("rating", "created", "author")

    @property
    def user(self):
        request = self.context.get("request")
        return request.user if request else None

    def get_children(self, parent):
        queryset = parent.get_children()
        serializer = ChildSerializer(queryset, many=True, read_only=True, context=self.context)
        return serializer.data

    def get_author_avatar(self, obj):
        if obj.author.murren_avatar and hasattr(obj.author.murren_avatar, "url"):
            return f"{settings.BACKEND_URL}{obj.author.murren_avatar.url}"

    def get_is_like(self, obj) -> bool:
        return obj.liked_murrens.filter(id=self.user.id).exists() if self.user else False

    def get_is_dislike(self, obj) -> bool:
        return obj.disliked_murrens.filter(id=self.user.id).exists() if self.user else False
