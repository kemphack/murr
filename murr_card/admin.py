from django.contrib import admin
from django_enum_choices.admin import EnumChoiceListFilter

from .models import MurrCard, Category


@admin.register(MurrCard)
class MurrCardAdmin(admin.ModelAdmin):
    date_hierarchy = 'timestamp'
    list_filter = [('status', EnumChoiceListFilter)]
    search_fields = ['title', 'owner__username']
    ordering = ('-rating',)
    list_display = ('title', 'rating', 'owner')


@admin.register(Category)
class MurrCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug')
    search_fields = ('name', 'slug')
